unit uEstagiarios;

interface

type
  TEstagiarios = class(TObject)
  private
    FNome    : String;
    FTelefone: string;
    FSetor   : String;
    FSalario : Real;

  public
    Constructor Create(p_nome, p_telefone,p_setor : string; p_salario : Real );
    property Nome : String read FNome write FNome;
    property Telefone : String read FTelefone write FTelefone;
    property Setor : String read FSetor write FSetor;
    property Salario : Real read FSalario write FSalario;
  end;


 var wg_estagiarios : Array of TEstagiarios;
 procedure AddEstagiario (p_nome, p_telefone, p_setor: string;  p_salario: Real);


implementation

{ TEstagiarios }

constructor TEstagiarios.Create(p_nome, p_telefone, p_setor: string;  p_salario: Real);
begin
  inherited Create();
  Nome     := p_nome;
  Telefone := p_telefone;
  Setor    := p_setor;
  Salario  := p_salario;
end;


procedure AddEstagiario (p_nome, p_telefone, p_setor: string;  p_salario: Real);
var
  wl_pos: Integer;
  wl_est: TEstagiarios;
 begin
   SetLength(wg_estagiarios, length(wg_estagiarios)+1);
   wl_pos := High(wg_estagiarios);
   wl_est := TEstagiarios.Create(p_nome, p_telefone, p_setor,p_salario);
   wg_estagiarios[wl_pos] := wl_est;
end;

end.
