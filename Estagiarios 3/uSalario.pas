unit uSalario;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, uPrincipal, uEstagiarios;

type
  TfmSalario = class(TForm)
  Panel1: TPanel;
  Label1: TLabel;
  btnMedia: TButton;
  btnListar: TButton;
  mmSalario: TMemo;
  procedure btnMediaClick(Sender: TObject);
  procedure btnListarClick(Sender: TObject);

  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  fmSalario: TfmSalario;

implementation

{$R *.dfm}

procedure TfmSalario.btnListarClick(Sender: TObject);
var
  wl_i: integer;
  wl_margin: integer;
  wl_marginT: integer;
  wl_marginSe: integer;
  wl_marginSa: integer;

begin
  if btnListar.Tag = 1 then
  exit;
  btnListar.Tag:= 1;

  for wl_i := 0 to  High(wg_estagiarios) do
      begin
      mmSalario.lines.add(
        wg_estagiarios[wl_i].Nome
        + '                             '
        +(wg_estagiarios[wl_i].Telefone)
        + '                             '
        + (wg_estagiarios[wl_i].Setor)
        + '                             '
        + (FloatToStr(wg_estagiarios[wl_i].Salario))
      );
      mmSalario.lines.add('__________________________________________________________________________________________');
      end;
end;

procedure TfmSalario.btnMediaClick(Sender: TObject);
var
  wl_media: real;
  wl_soma: real;
  wl_i: integer;
  wl_a: integer;
begin
  wl_soma:=0;
  for wl_i := 0 to length(wg_estagiarios) - 1 do
    wl_soma:= wl_soma + wg_estagiarios[wl_i].Salario;

  wl_media:= wl_soma / length(wg_estagiarios);

  showMessage('R$ '+FloatToStr(wl_media)+',00');
end;

end.
