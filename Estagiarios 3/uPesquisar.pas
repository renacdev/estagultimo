unit uPesquisar;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, dxGDIPlusClasses, uPrincipal, uEstagiarios;

type
  TfmPesquisar = class(TForm)
  Panel1: TPanel;
  Label1: TLabel;
  btnPesquisar: TButton;
  Image1: TImage;
  edPesquisar: TEdit;
  btnLimpar: TButton;
  procedure btnPesquisarClick(Sender: TObject);
  procedure FormActivate(Sender: TObject);
  procedure btnLimparClick(Sender: TObject);

  private
    { Private declarations }
  public
  wg_lbNome: TLabel;
  wg_lbTelefone: TLabel;
  wg_lbSetor: TLabel;
  wg_lbSalario: TLabel;
  wg_margin: integer;
  wg_marginT: integer;
  wg_marginSe: integer;
  wg_marginSa: integer;
end;

var
  fmPesquisar: TfmPesquisar;

implementation

{$R *.dfm}

procedure TfmPesquisar.btnLimparClick(Sender: TObject);
begin
  edPesquisar.Text := '';
  edPesquisar.setfocus;
end;

procedure TfmPesquisar.btnPesquisarClick(Sender: TObject);
var
  wl_margin: integer;
  wl_a: integer;

//Limpar tela a cada pesquisa
begin
  if btnPesquisar.Tag=1 then
    begin
      if lbNome <> nil then
        lbNome.free;
      if lbSetor <> nil then
        lbSetor.free;
      if lbTelefone <> nil then
        lbTelefone.Free;
      if lbTelefone <> nil then
        lbSalario.free;
    end;
//************************************************

  if edPesquisar.text = ''  then
    showMessage('Digite um Estagiário');

  wl_margin:= 100;
  wg_marginT:= 100;
  wg_MarginSe:= 100;
  wg_MarginSa:= 100;

  for wl_a := 0 to length(wg_estagiarios) - 1 do
    if edPesquisar.text = (wg_estagiarios[wl_a].Nome) then
      begin
        lbNome := Tlabel.create(application);
        lbNome.Parent := fmPesquisar;
        lbNome.Caption:= wg_estagiarios[wl_a].Nome;
        lbNome.Top := wl_margin;
        lbNome.Left := 130;

        lbTelefone := Tlabel.create(application);
        lbTelefone.Parent := fmPesquisar;
        lbTelefone.Caption:= wg_estagiarios[wl_a].Telefone;
        lbTelefone.Top := wg_marginT;
        lbTelefone.Left := 200;

        lbSetor := Tlabel.create(application);
        lbSetor.Parent := fmPesquisar;
        lbSetor.Caption:= wg_estagiarios[wl_a].Setor;
        lbSetor.Top := wg_marginSe;
        lbSetor.Left := 300;

        lbSalario := Tlabel.create(application);
        lbSalario.Parent := fmPesquisar;
        lbSalario.Caption:= (FloatToStr(wg_estagiarios[wl_a].Salario));
        lbSalario.Top := wg_marginSa;
        lbSalario.Left := 400;
     end;
  btnPesquisar.Tag:=1;
end;

procedure TfmPesquisar.FormActivate(Sender: TObject);
begin
  btnPesquisar.Tag:= 0;
end;


end.
