unit uListar;

interface

uses
  Windows, Messages, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, uEstagiarios;

type
  TfmListar = class(TForm)
  Panel1: TPanel;
  Label1: TLabel;
  btnListar: TButton;
  mmListar: TMemo;
  Label2: TLabel;
  Label3: TLabel;
  Label4: TLabel;
  Label5: TLabel;
  procedure btnListarClick(Sender: TObject);
  procedure FormActivate(Sender: TObject);
  procedure FormClose(Sender: TObject; var Action: TCloseAction);

  private
    { Private declarations }
  public
end;

var
  fmListar: TfmListar;

implementation

uses uPrincipal, Sysutils;

{$R *.dfm}


procedure TfmListar.btnListarClick(Sender: TObject);
var
  wl_i: integer;
  wl_a: integer;
  wl_x: integer;
  wl_margin: integer;
  wl_marginT: integer;
  wl_marginSe: integer;
  wl_marginSa: integer;

begin
  if btnListar.Tag = 1 then
  exit;
  btnListar.Tag:= 1;

  wg_n:=6;
  wl_margin:= 50;

  for wl_i := 0 to  High(wg_estagiarios) do
    begin
      mmListar.lines.add(
        wg_estagiarios[wl_i].Nome
        + '                             '
        +(wg_estagiarios[wl_i].Telefone)
        + '                             '
        + (wg_estagiarios[wl_i].Setor)
        + '                             '
        + (FloatToStr(wg_estagiarios[wl_i].Salario))
      );
      mmListar.lines.add('__________________________________________________________________________________________');

//CRIAR A LISTAGEM POR MEIO DE LABEL
//      lbNome:= Tlabel.create(application);
//      lbNome.Parent := fmListar;
//      lbNome.Caption:= wg_estagiarios[wl_i].Nome;
//      lbNome.Top := wl_margin + 20;
//      lbNome.Left := 100;
//      wl_margin:= wl_margin + 20;
//
//      lbTelefone := Tlabel.create(application);
//      lbTelefone.Parent := fmListar;
//      lbTelefone.Caption:=wg_estagiarios[wl_i].Telefone;
//      lbTelefone.Top := wl_margin;
//      lbTelefone.Left := 225;
//      wl_marginT:= wl_marginT + 20;
//
//      lbSetor := Tlabel.create(application);
//      lbSetor.Parent := fmListar;
//      lbSetor.Caption:= wg_estagiarios[wl_i].Setor;
//      lbSetor.Top := wl_margin;
//      lbSetor.Left := 345;
//      wl_marginSe:= wl_marginSe + 20;
//
//      lbSalario := Tlabel.create(application);
//      lbSalario.Parent := fmListar;
//      lbSalario.Caption:= FloatToStr(wg_estagiarios[wl_i].Salario);
//      lbSalario.Top := wl_margin;
//      lbSalario.Left := 470;
//      wl_marginSa:= wl_marginSa + 20;
//    end;
//***************************************************************************

end;
end;

procedure TfmListar.FormActivate(Sender: TObject);
begin
  btnListar.Tag:= 0;
end;

procedure TfmListar.FormClose(Sender: TObject; var Action: TCloseAction);
var
  x: integer;
begin
  btnListar.Tag:= 0;
  for x := 0 to 15 do
    begin
    end;
end;

end.
